import random
import numpy as np 
from PIL import Image
from glob import glob

import torch
from torchvision import transforms
from torch.utils.data import random_split
from torch.utils.data.dataset import Dataset

class CustomDataset(Dataset):
    # function is where the initial logic happens like reading a csv, assigning transforms etc.
    def __init__(self, path, transform=None):
        self.transform = transform
        self.toTensor = transforms.ToTensor()
        self.path = path
        self.images = glob(path + "/*")
        
    # function returns the data and labels. This function is called from dataloader like this:
    # img, label = CustomDataset.__getitem__(99)  # For 99th item
    def __getitem__(self,index):
        image = Image.open(self.path + "/{}.jpg".format(index)) # .convert('RGB')
        label = Image.open(self.path + "/{}#mask.jpg".format(index)) # .convert('L')
        
        seed = np.random.randint(2147483647) # make a seed with numpy generator 
        
        random.seed(seed) # apply this seed to img tranfsorms
        torch.manual_seed(seed)
        if self.transform:
            image = self.transform(image)
            
        random.seed(seed) # apply this seed to target tranfsorms
        torch.manual_seed(seed) # нужен ли второй раз?
        if self.transform:
            label = self.transform(label)

#         label = torch.ByteTensor(np.array(label))
#         image = self.toTensor(image)
#         label = self.toTensor(label)
        return image, label

    def __len__(self):
        return int(len(self.images)/2) # of how many examples in dataset
    
def split_dataset(master_dataset, percentage_of_train = 0.8):
    train_size = int(0.8 * len(master_dataset))
    test_size = int((len(master_dataset) - train_size)/2)
    val_size = test_size
    train_dataset, test_dataset, val_dataset = random_split(master_dataset, [train_size, test_size, val_size])
    return train_dataset, test_dataset, val_dataset

# # Set train and valid directory paths
# train_directory = 'train'
# valid_directory = 'test'
 
# # Batch size
# bs = 32
 
# # Number of classes
# num_classes = 10
 
# # Load Data from folders
# data = {
#     'train': datasets.ImageFolder(root=train_directory, transform=image_transforms['train']),
#     'valid': datasets.ImageFolder(root=valid_directory, transform=image_transforms['valid']),
#     'test': datasets.ImageFolder(root=test_directory, transform=image_transforms['test'])
# }
 
# # Size of Data, to be used for calculating Average Loss and Accuracy
# train_data_size = len(data['train'])
# valid_data_size = len(data['valid'])
# test_data_size = len(data['test'])
 
# # Create iterators for the Data loaded using DataLoader module
# train_data = DataLoader(data['train'], batch_size=bs, shuffle=True)
# valid_data = DataLoader(data['valid'], batch_size=bs, shuffle=True)
# test_data = DataLoader(data['test'], batch_size=bs, shuffle=True)
 
# # Print the train, validation and test set data sizes
# train_data_size, valid_data_size, test_data_size