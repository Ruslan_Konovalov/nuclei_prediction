from glob import glob
from PIL import Image
import numpy as np 
import pandas as pd 
import os
import random
import sys

class Preprocess:
    """
    python3 preprocess.py input_folder output_folder
    
    Input 2560 x 1920
    Output 192 crops 160 х 160
    """
    def __init__(self,img_size,crop_size,input_folder,output_folder):
        self.img_size = img_size
        self.crop_size = crop_size
        self.path_to_source = input_folder
        self.output_folder = output_folder
        
    def dots_list(self):
        width,height = self.crop_size
        x,y = [],[]
        a,b = 0,0
        while a < self.img_size[0]:
            x.append(a)
            a += width
        while b < self.img_size[1]:
            y.append(b)
            b += height
        coordinate = [(a, b) for a in x for b in y]
        return coordinate
    
    def check_dataset_length(self):
        '''
        Dataset length should be EVEN (100 not 101 or 99)
        '''
        if len(glob(self.path_to_source+'/*'))%2==0:
            return True
        else:
            return False
        
    def image_preprocess(self):
        
        os.makedirs('{}/{}'.format(os.getcwd(),self.output_folder), exist_ok=True)
#         os.makedirs('{}/{}'.format(os.getcwd(),'{}/validation'.format(self.output_folder)), exist_ok=True)
#         os.makedirs('{}/{}'.format(os.getcwd(),'{}/test'.format(self.output_folder)), exist_ok=True)

        name = 0
        original_images = glob('{}/*#original*'.format(self.path_to_source))
#         validation_images = glob('{}/*#validation*'.format(self.path_to_source))
#         test_images = glob('{}/*#test*'.format(self.path_to_source))
#         label_images = glob('{}/*#label.jpg'.format(self.path_to_source))
#         for validation_image in validation_images:
#             validation_img = Image.open(validation_image)
#             validation_img.save('{}/{}.jpg'.format(os.getcwd()+'/{}/validation'.format(self.output_folder),str(name)))
#         for test_image in test_images:
#             test_img = Image.open(test_image)
#             test_img.save('{}/{}.jpg'.format(os.getcwd()+'/{}/test'.format(self.output_folder),str(name)))
        for original_image in original_images:
            original_img = Image.open(original_image)
            original_img = Image.fromarray(np.asarray(original_img).transpose(2,0,1)[2]) # взял один канал
            label_img = Image.open(original_image.replace('original','label'))
            if len(np.asarray(label_img).shape)!=2:
                label_img = Image.fromarray(np.asarray(label_img).transpose(2,0,1)[2]) # взял один канал
            #проверка на размер
            coordinate = self.dots_list()
            width, height = self.crop_size
            for x,y in coordinate:
                area = [x,y,x+width,y+height]
                original_image_crop = original_img.crop(area)
                label_image_crop = label_img.crop(area)
                # removed empty masks. because in training I got loss nan 
                if np.array(label_image_crop).sum()!=0:
                    original_image_crop.save('{}/{}.jpg'.format(os.getcwd()+'/{}'.format(self.output_folder),str(name)))
                    label_image_crop.save('{}/{}#mask.jpg'.format(os.getcwd()+'/{}'.format(self.output_folder),str(name)))
                    name += 1

                
if __name__ == "__main__":
    #path_to_source = 'source'
    input_folder = sys.argv[1]
    output_folder = sys.argv[2]
    img_size = (2560,1920)
    crop_size = (160,160)
    obj = Preprocess(img_size,crop_size,input_folder,output_folder)
    obj.image_preprocess()
    if not obj.check_dataset_length():
        last_id = len(glob('{}/{}/*#mask.jpg'.format(os.getcwd(),output_folder)))-1
        os.remove('{}/{}.jpg'.format(os.getcwd()+'/{}'.format(output_folder),str(last_id)))
        os.remove('{}/{}#mask.jpg'.format(os.getcwd()+'/{}'.format(output_folder),str(last_id)))
        print(output_folder, last_id)
        