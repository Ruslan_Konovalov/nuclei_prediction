import os
import sys
import random
import time
import numpy as np 
import pandas as pd
from glob import glob
from PIL import Image
import matplotlib.pyplot as plt

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import torchvision
import torchvision.datasets
from torchvision import transforms
from torchvision.utils import make_grid
from torch.autograd import Variable
from torch.utils import data
from torch.utils.data import DataLoader
from torch.utils.data.dataset import Dataset
from torch.utils.data.sampler import RandomSampler, SequentialSampler


from augmentation import TRANSFORM
from custom_dataset import CustomDataset, split_dataset
from unet import BaseConv, DownConv, UpConv, UNet
from loss_functions import SoftDiceLoss

def main(input_folder, BATCH_SIZE, EPOCHS, debug):

    master_dataset = CustomDataset(input_folder,transform=TRANSFORM['train'])
    train_dataset, test_dataset, val_dataset = split_dataset(master_dataset, percentage_of_train=0.8)

    train_loader = torch.utils.data.DataLoader(dataset=train_dataset,batch_size=BATCH_SIZE,shuffle=True,num_workers=0)

    validation_loader = torch.utils.data.DataLoader(dataset=val_dataset,batch_size=BATCH_SIZE,shuffle=True,num_workers=0)

    test_loader = torch.utils.data.DataLoader(dataset=test_dataset,batch_size=BATCH_SIZE,shuffle=True,num_workers=0)

    LEARNING_RATE = 0.0001
    os.makedirs(os.getcwd()+'/weights', exist_ok=True)
    OUT_PATH = 'weights'

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model = UNet(in_channels=1,out_channels=64,
                 n_class=1,kernel_size=3,
                 padding=1,stride=1).to(device)

    optimizer = optim.RMSprop(model.parameters(), lr=LEARNING_RATE, momentum=0.99)
    criterion = SoftDiceLoss()
    criterion_name = repr(criterion)[:-2]

    # input size (Batch, Number Channels, height, width)

    since = time.time()
    train_id = random.randint(0,100)
    file = open("TrainID: {} | Date: {}.txt".format(train_id,time.strftime("%m-%d-%H:%M", time.localtime(since))),"w") 
    file.write('BATCH_SIZE: {}, EPOCHS: {}, CRITERION: {}'.format(BATCH_SIZE,EPOCHS,criterion_name))

    # Loss and Accuracy within the epoch
    train_loss = 0.0
    train_acc = 0.0

    valid_loss = 0.0
    valid_acc = 0.0

    history=dict(zip(['avg_train_loss', 'avg_valid_loss', 'avg_train_acc', 'avg_valid_acc'], [[],[],[],[]]))

    for epoch in range(EPOCHS):
        epoch_start = time.time()
        model.train()
        optimizer.zero_grad()
        # train
        for inputs, labels in train_loader:
            inputs, labels = inputs.to(device), labels.to(device)
            output = model(inputs.float())
            loss = criterion(output, labels) 
            loss.backward()    
            optimizer.step()
            train_loss += float(loss.item())
            
            if debug:
                file.write('Training')
                file.write('\n')
                file.write('loss: {} | type_loss: {}'.format(loss, type(loss)))
                file.write('\n')
                file.write('train_loss: {} | type_train_loss: {}'.format(train_loss, type(train_loss)))
                file.write('\n')
            
        # validate
        else:
            model.eval()
            with torch.no_grad():
                for inputs, labels in validation_loader:
                    inputs, labels = inputs.to(device), labels.to(device)
                    #output = model.forward(inputs)
                    output = model(inputs)
                    loss = criterion(output, labels)
                    valid_loss += loss.item() # inputs.size(0)
                    
                    if debug:
                        file.write('Validation')
                        file.write('\n')
                        file.write('loss: {} | type_loss: {}'.format(loss, type(loss)))
                        file.write('\n')
                        file.write('valid_loss: {} | type_valid_loss: {}'.format(valid_loss, type(valid_loss)))
                        file.write('\n')
                    
            # Find average training/valid loss
            avg_train_loss = train_loss/len(train_loader) 
            avg_valid_loss = valid_loss/len(validation_loader) 
#             # Find average training/valid accuracy
#             avg_train_acc = train_acc/float(len(train_loader))
#             avg_valid_acc = valid_acc/float(len(validation_loader))
            
            history['avg_train_loss'].append(avg_train_loss)
            history['avg_valid_loss'].append(avg_valid_loss)
            #history['avg_train_acc'].append(avg_train_acc)
            #history['avg_valid_acc'].append(avg_valid_acc)
            file.write(repr(history))
            file.write('\n')
                
            torch.save(model.state_dict(), OUT_PATH + '/TrainID:{}_epoch_{}.pt'.format(train_id,epoch))

    time_elapsed = time.time() - since
    file.write('Training complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
    file.write('\n')
    file.write(repr(history))
    file.write('\n')
    file.close()

    plt.plot(history['avg_train_loss'], label='Training loss')
    plt.plot(history['avg_valid_loss'], label='Validation loss')
    plt.savefig('train.jpg')
    
if __name__ == "__main__":
    input_folder = sys.argv[1]
    BATCH_SIZE = int(sys.argv[2])
    EPOCHS = int(sys.argv[3])
    debug = sys.argv[4]
    main(input_folder, BATCH_SIZE, EPOCHS, debug)
