from torchvision import transforms

TRANSFORM = {
    'train': transforms.Compose([
#     transforms.Resize(54),
#     transforms.ColorJitter(
#         brightness=0.1*torch.randn(1),
#         contrast=0.1*torch.randn(1),
#         saturation=0.1*torch.randn(1),
#         hue=0.1*torch.randn(1)
#     ),
#         transforms.RandomCrop(size = (60,60)),
#         transforms.RandomResizedCrop(224),
        transforms.RandomHorizontalFlip(),
        transforms.RandomVerticalFlip(),
        transforms.RandomRotation(90),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485], std=[0.229] ), # for 1 channel
#         transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]) # for 3 channel
    ]),
    'val': transforms.Compose([
        transforms.Resize(160),
        transforms.CenterCrop(144),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485], std=[0.229] ), # for 1 channel
#         transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]) # for 3 channel
    ]),
}